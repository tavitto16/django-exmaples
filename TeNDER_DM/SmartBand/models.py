from django.db import models


class SmartBandModel(models.Model):
	userProfileID = models.CharField(max_length=30)
	client      = models.CharField(max_length=30)
	sensorID    = models.CharField(max_length=30)
	timestamp   = models.DateField()
	spo2        = models.CharField(max_length=30)
	heart_rate  = models.CharField(max_length=30)
	number_steps  = models.CharField(max_length=30)
	accelerometer = models.CharField(max_length=256)
	barometer     = models.CharField(max_length=30)
	blood_presure = models.CharField(max_length=30)
	gyroscope     = models.CharField(max_length=256)

	def __str__(self):
		return self.userProfileID

		

#from rest_framework_swagger.views import get_swagger_view
# Create your models here.

#schema_view = get_swagger_view(title = "Smartband API")

