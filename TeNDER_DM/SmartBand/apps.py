from django.apps import AppConfig


class SmartbandConfig(AppConfig):
    name = 'SmartBand'
