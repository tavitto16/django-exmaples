from django.shortcuts import render
from rest_framework_swagger.views import get_swagger_view
from SmartBand.models import SmartBandModel
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
import json
#schema_view
from django.views.generic import View, TemplateView, DetailView, ListView, CreateView, UpdateView, DeleteView

# @csrf_exempt   #remove for authentication
# def SmartBandProcessor(request):
# 	# login if required to store data securely
# 	data_to_save = SmartBandModel()
# 	if request.method == 'POST':
# 		data = request.body.decode("utf-8")
# 		received_json = json.loads(data)
# 		if received_json['userProfileID']:
# 			data_to_save.userProfileID = received_json['userProfileID']
# 		if received_json['client']:
# 			data_to_save.client        = received_json['client']
# 		if received_json['sensorID']:
# 			data_to_save.sensorID      = received_json['sensorID']
# 		if received_json['timestamp']:
# 			data_to_save.timestamp     = received_json['timestamp']
# 		if received_json['spo2']:
# 			data_to_save.spo2          = received_json['spo2']
# 		if  received_json['heart_rate']:
# 			data_to_save.heart_rate    = received_json['heart_rate']
# 		if received_json['number_steps']:
# 			data_to_save.number_steps  = received_json['number_steps']
# 		if received_json['accelerometer']:
# 			data_to_save.accelerometer = received_json['accelerometer']
# 		if received_json['barometer']:
# 			data_to_save.barometer     = received_json['barometer']
# 		if received_json['blood_presure']:
# 			data_to_save.blood_presure = received_json['blood_presure']
# 		if received_json['gyroscope']:
# 			data_to_save.gyroscope     = received_json['gyroscope']
# 		data_to_save.save()
# 		return HttpResponse("Saved")

	#if request.method == 'GET' : 






class SmartBandModelList(ListView):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(SmartBandModelList, self).dispatch(request, *args, **kwargs)
    models = SmartBandModel
    def get_queryset(self):
    	return SmartBandModel.objects.all()

class SmartBandModelCreate(CreateView):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(SmartBandModelCreate, self).dispatch(request, *args, **kwargs)
    fields=('userProfileID','client','sensorID','timestamp','spo2','heart_rate','number_steps','accelerometer','barometer','blood_presure','gyroscope')
    models = SmartBandModel

#TO DO
# Create the views for the 

# @swagger_auto_schema( 
# 			operation_description="apiview post description override",
# 			request_body=openapi.Schema(
# 				type=openapi.TYPE_OBJECT,
# 				required=['username'],
# 				properties={
# 				'username': openapi.Schema(type=openapi.TYPE_STRING) },
# 				),
# 			security=[],
# 			tags=['Users'],
# 			)


	

